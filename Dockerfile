FROM node:16-alpine3.11 as builder

ENV NODE_ENV build

USER root
WORKDIR /app

COPY . /app

RUN yarn
RUN yarn build 

WORKDIR /app/build
RUN yarn --prod

FROM node:16-alpine3.11 as runner


# ensure we only use apk repositories over HTTPS (altough APK contain an embedded signature)
RUN echo "https://alpine.global.ssl.fastly.net/alpine/v$(cut -d . -f 1,2 < /etc/alpine-release)/main" > /etc/apk/repositories \
    && echo "https://alpine.global.ssl.fastly.net/alpine/v$(cut -d . -f 1,2 < /etc/alpine-release)/community" >> /etc/apk/repositories

# The user the app should run as
ENV APP_USER=app
# The home directory
ENV APP_DIR="/$APP_USER"
# Where persistent data (volume) should be stored
ENV DATA_DIR "$APP_DIR/data"
# Where configuration should be stored
ENV CONF_DIR "$APP_DIR/conf"

# Add custom user and setup home directory
RUN addgroup -S $APP_USER && adduser -s /bin/true -u 1001 -S -D -h $APP_DIR $APP_USER -G $APP_USER \
    && mkdir "$DATA_DIR" "$CONF_DIR" \
    && chown -R "$APP_USER" "$APP_DIR" "$CONF_DIR" \
    && chmod 700 "$APP_DIR" "$DATA_DIR" "$CONF_DIR"

# Default directory is /app
WORKDIR $APP_DIR

# Copy
COPY hardener.sh ./
RUN chmod 755 hardener.sh && ./hardener.sh

# Enforce User
USER $APP_USER:$APP_USER

COPY --from=builder /app/build/package*.json /app/
COPY --from=builder /app/build/node_modules/ /app/node_modules/
COPY --from=builder /app/build/ /app/

CMD ["node", "server.js"]
