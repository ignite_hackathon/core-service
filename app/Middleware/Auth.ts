import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import jwt_decode from 'jwt-decode'
import jwks from 'jwks-rsa'
import jwt from 'jsonwebtoken'

const audience = 'https://auth-service.nubemotic.com/'
const issuer = 'https://ignite-hackathon.eu.auth0.com/'
const algorithms = ['RS256']

const jwksClient = jwks({
  cache: true,
  rateLimit: true,
  cacheMaxAge: 600000,
  jwksUri: 'https://ignite-hackathon.eu.auth0.com/.well-known/jwks.json',
})

const isValidToken = (token, signingKey) => {
  return new Promise((resolve) => {
    jwt.verify(token, signingKey, { audience, issuer, algorithms }, async function (err, decoded) {
      if (err) {
        resolve(false)

        return
      } else {
        resolve(true)
      }
    })
  })
}

export default class Auth {
  public async handle({ request, response }: HttpContextContract, next: () => Promise<void>) {
    try {
      const token = (request.headers().authorization as any).split('Bearer ')[1] || null
      const tokenHeader: any = jwt_decode(token, { header: true })
      const kid = tokenHeader.kid
      const key = await jwksClient.getSigningKey(kid)
      const signingKey = key.getPublicKey()

      if (await isValidToken(token, signingKey)) {
        return await next()
      }

      response.unauthorized({ error: 'Must be logged in' })
      return
    } catch (error) {
      response.unauthorized({ error: 'Must be logged in' })
      return
    }
  }
}
