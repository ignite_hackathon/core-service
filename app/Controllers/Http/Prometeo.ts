const apiKey = process.env.PROMETEO_API_KEY
const baseUrl = process.env.PROMETEO_BASE_URL
import got from 'got'

const loginEndpoint = `${baseUrl}/login/`
const listProviderEndpoint = `${baseUrl}/provider/`
const accountsEndpoint = `${baseUrl}/account/`

export const listMovements = async (accountNumber, key, currency, dateStart, dateEnd) => {
  try {
    const finalEndpoint = `${accountsEndpoint}${accountNumber}/movement/?key=${key}&currency=${currency}&date_start=${dateStart}&date_end=${dateEnd}`

    const { body } = await got.get(finalEndpoint, {
      responseType: 'json',
      headers: { 'X-API-Key': apiKey },
    })
    return body
  } catch (error) {
    console.log(error)
  }
}

export const listAccounts = async (key) => {
  try {
    const { body } = await got.get(`${accountsEndpoint}?key=${key}`, {
      responseType: 'json',
      headers: { 'X-API-Key': apiKey },
    })
    return body
  } catch (error) {
    console.log(error)
  }
}

export const listProvider = async () => {
  try {
    const { body } = await got.get(listProviderEndpoint, {
      responseType: 'json',
      headers: { 'X-API-Key': apiKey },
    })
    return body
  } catch (error) {
    console.log(error)
  }
}

export const login = async (provider, username, password, type) => {
  try {
    const { body } = await got.post(loginEndpoint, {
      form: {
        provider,
        username,
        password,
        type,
      },
      responseType: 'json',
      headers: { 'X-API-Key': apiKey },
    })
    return body
  } catch (error) {
    console.log(error)
  }
}
