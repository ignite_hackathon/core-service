import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import * as PrometeoService from './Prometeo'
import { runClassifier } from './Classifier'

import NodeCache from 'node-cache'
const myCache = new NodeCache({ stdTTL: 21600, checkperiod: 120 })

var crypto = require('crypto')

const exampleMasterPassword = crypto
  .createHash('sha256')
  .update('thisIsMasterPassword')
  .digest('base64')

const exampleUid = crypto
  .createHash('sha256')
  .update('auth0|619b0642470d070069329810')
  .digest('base64')

const masterPasswordPlusUid = crypto
  .createHash('sha256')
  .update(`${exampleMasterPassword}${exampleUid}`)
  .digest('base64')

const exampleUser = {
  provider: 'test',
  username: '12345',
  password: 'gfdsa',
  type: '',
}

const localDatabase = []
localDatabase[masterPasswordPlusUid] = {
  id: exampleUid,
  data: exampleUser,
}

const getKey = async (masterPassword, uid) => {
  const masterPasswordPlusUid = crypto
    .createHash('sha256')
    .update(`${masterPassword}${uid}`)
    .digest('base64')

  const decryptedData = localDatabase[masterPasswordPlusUid].data

  const { key } = (await PrometeoService.login(
    decryptedData.provider,
    decryptedData.username,
    decryptedData.password,
    decryptedData.type
  )) as any
  return key
}

export default class CoreController {
  public async getAccounts(ctx: HttpContextContract) {
    return await PrometeoService.listAccounts(await getKey(exampleMasterPassword, exampleUid))
  }
  public async getAllMovementsClassified(ctx: HttpContextContract) {
    const allMovements = await this.getAllMovements(ctx)

    console.log('Total cuentas a clasificar: ', allMovements.length)
    const allMovementsClassified = []
    for (const account of allMovements) {
      console.log('Obteniendo movimientos de cuenta numero ', account.accountNumber)
      console.log('Mandando a clasificar ', account.movement.length, ' movimientos.')
      const cache = myCache.get(`${account.accountNumber}`)
      if (cache == undefined) {
        console.log('Cache not found getting from classifier')
        const result = await runClassifier(account.movement)
        allMovementsClassified.push({ accountNumber: account.accountNumber, movement: result })
        myCache.set(`${account.accountNumber}`, result, 21600)
      } else {
        console.log('Cache found!')
        allMovementsClassified.push({ accountNumber: account.accountNumber, movement: cache })
      }
    }
    return allMovementsClassified
  }

  public async getAllMovements(ctx: HttpContextContract) {
    const responseAccounts = await this.getAccounts(null)
    const accounts = (responseAccounts as any).accounts
    const allMovements = []
    const dateEnd = '01/11/2021'
    const dateStart = '01/08/2021'

    for (const account of accounts) {
      const responseMovement = await PrometeoService.listMovements(
        account.number,
        await getKey(exampleMasterPassword, exampleUid),
        account.currency,
        dateStart,
        dateEnd
      )

      allMovements.push({
        accountNumber: account.number,
        movement: (responseMovement as any).movements,
      })
    }
    return allMovements
  }

  public async getSingleAccountMovements(ctx: HttpContextContract) {
    const accountNumber = ctx.request.params().account
    const responseAccounts = await this.getAccounts(null)
    const allMovements = []
    const accounts = (responseAccounts as any).accounts
    const accountSelected = accounts.find((account) => account.number === accountNumber)
    if (!accountSelected) {
      return 'Account not found'
    }

    const dateEnd = '01/11/2021'
    const dateStart = '01/08/2021'

    const responseMovement = await PrometeoService.listMovements(
      accountSelected.number,
      await getKey(exampleMasterPassword, exampleUid),
      accountSelected.currency,
      dateStart,
      dateEnd
    )

    allMovements.push({
      accountNumber: accountSelected.number,
      movement: (responseMovement as any).movements,
    })
    return allMovements
  }

  public async getProviders(ctx: HttpContextContract) {
    const providers = await PrometeoService.listProvider()
    return providers
  }
}
