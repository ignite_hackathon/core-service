import got from 'got'

const endpointClassifier =
  process.env.NODE_ENV === 'production'
    ? 'http://classifier-service/get'
    : 'https://classifier.nubemotic.com/get'

export const runClassifier = async (movements: string[]) => {
  let totalAClassificar = movements.length
  let movimientosActuales = 0
  console.log('Total a clasificar: ', totalAClassificar, ' movimientos.')
  try {
    let movementToClassify = []
    let resultMovementsClassfied = []
    for (const move of movements) {
      movementToClassify.push(move)
      if (movementToClassify.length > 1000) {
        movimientosActuales += movementToClassify.length
        console.log('Clasificando ', movimientosActuales, ' de ', totalAClassificar)
        const result = await externalClassify(movementToClassify)
        resultMovementsClassfied = [...resultMovementsClassfied, ...result.moves]
        console.log('Agregado')
        movementToClassify = []
      }
    }
    return resultMovementsClassfied
  } catch (error) {
    console.log(error)
  }
  return 'Error'
}

const externalClassify = async (movements) => {
  try {
    const { body } = await got.post(endpointClassifier, {
      retry: { limit: 6, methods: ['GET', 'POST'] },
      json: {
        movements,
      },
      responseType: 'json',
    })
    return body
  } catch (error) {
    console.log('error')
  }
  return 'Error'
}
