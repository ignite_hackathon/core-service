import crypto = require('crypto')

const algorithm = 'aes-256-ctr'

const iv = crypto.randomBytes(16)

export const encrypt = (text, secretKey) => {
  console.log('Encrypting:', text, ' with', secretKey)
  const cipher = crypto.createCipheriv(algorithm, secretKey, iv)

  const encrypted = Buffer.concat([cipher.update(text), cipher.final()])

  return {
    iv: iv.toString('hex'),
    content: encrypted.toString('hex'),
  }
}

export const decrypt = (hash, secretKey) => {
  console.log('Desencriptando:', hash)
  const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'))
  console.log('Listo!')

  const decrpyted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, 'hex')),
    decipher.final(),
  ])

  return decrpyted.toString()
}
